//
// Created by Meritis Local on 08/10/2019.
//

#include <utility>
#include "Goldbach.h"

bool isPrime(int n)
{
    if (n <= 1) return false;
    if (n == 2) return true;

    for (int i = 2; i * i <= n; i++)
        if (n % i == 0)
            return false;
    return true;
}

int closestPrime(int num) {
    bool primeFound = true;
    int primeCandidate = 1;
    for (primeCandidate = num - 1; primeCandidate >= 1; primeCandidate--) {
        primeFound = isPrime(primeCandidate);
        if (primeFound) {
            return primeCandidate;;
        }
    }
}

std::pair<int, int> Goldbach(int num) {
    int closestPrimeNumber = closestPrime(num);
    int couple = num-closestPrimeNumber;
    while (! isPrime(couple)) {
        closestPrimeNumber = closestPrime(closestPrimeNumber);
        couple = num - closestPrimeNumber;
    }

    return std::make_pair(couple, closestPrimeNumber);
}
