//
// Created by Meritis Local on 08/10/2019.
//

#include <string>
#include "gtest/gtest.h"
#include "../src/Goldbach.h"

TEST(TestGoldbach, testCase)
{
    EXPECT_EQ(std::make_pair(2,2),Goldbach(4));
    EXPECT_EQ(std::make_pair(3,3),Goldbach(6));
    EXPECT_EQ(std::make_pair(3,5),Goldbach(8));
    EXPECT_EQ(std::make_pair(3,7),Goldbach(10));
    EXPECT_EQ(std::make_pair(3,13),Goldbach(16));
}